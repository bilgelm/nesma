from setuptools import setup

setup(name='nesma',
      version='0.1.0',
      description='Nonlocal estimation of multispectral magnitudes',
      url='http://gitlab.com/bilgelm/nesma',
      author='Murat Bilgel, Mustapha Bouhrara',
      author_email='murat.bilgel@nih.gov, mustapha.bouhrara2@nih.gov',
      license='MIT',
      packages=['nesma'],
      install_requires=['numpy','tqdm'],
      zip_safe=False,
      setup_requires=['pytest-runner'],
      tests_require=['pytest','awscli'],
      extras_require={'nipype': ['nipype']},
)
