import unittest
import os
from shutil import rmtree

class TestNipype(unittest.TestCase):
    '''
    These tests simply check if the individual modules run.
    Outputs/pipeline specifications are not necessarily sensible.
    '''
    def setUp(self):
        wd = os.path.dirname(os.path.abspath(__file__))
        self.data_dir = os.path.join(wd, 'test_data')

        if not os.path.isdir(self.data_dir):
            os.mkdir(self.data_dir)

        #ds_list = ['ds000204', # not really working...
        #           'ds001420',
        #           'ds001421', # missing mris
        #           'ds001705', # large
        #           'ds002041'] # v large

        ds = 'ds001420'
        target_dir = os.path.join(self.data_dir,ds)

        self.pet = os.path.join(target_dir, 'sub-01', 'ses-01', 'pet',
                                'sub-01_ses_01_pet.nii.gz')
        self.mri = os.path.join(target_dir, 'sub-01', 'ses-01', 'anat',
                                'sub-01_ses-01_T1w.nii')

        # obtain files if not found
        if not os.path.isfile(self.mri) or \
           not os.path.isfile(self.pet):
            import subprocess, re, json

            subprocess.call(['aws','s3','sync','--no-sign-request',
                             's3://openneuro.org/' + ds, target_dir])

    #def tearDown(self):
            # remove the tests_output directory
            #rmtree("tests/test_output")

    def test_nesma(self):
        from nipype import Node, Workflow
        from nesma.nipype_wrapper import NESMA
        from nipype.interfaces import fsl
        from nipype.interfaces.base import traits
        from nipype.pipeline.engine import MapNode

        n_procs = 22

        timesplit = Node(fsl.Split(in_file = self.pet, dimension='t'),
                         name="timesplit")
        resample = MapNode(fsl.ApplyXFM(apply_xfm=traits.Undefined,
                                        apply_isoxfm = 3),
                           iterfield=['in_file','reference'],
                           name="resample")
        timemerge = Node(fsl.Merge(dimension='t'), name="timemerge")

        brainmask = Node(fsl.ImageMaths(op_string='-Tmean -thrp 20 -bin -mul 1'),
                         name="brainmask")

        nesma = Node(NESMA(in_file = self.pet,
                           window_half_size = [7,6,5],
                           thresh = 0.1), name="nesma")

        wf = Workflow(name="wf", base_dir="tests/test_output")
        wf.connect([
            (timesplit, resample, [('out_files','in_file'),
                                   ('out_files','reference')]),
            (resample, timemerge, [('out_file','in_files')]),

            (timemerge, brainmask, [('merged_file','in_file')]),

            (timemerge, nesma, [('merged_file','in_file')]),
            (brainmask, nesma, [('out_file','mask_file')]),
        ])

        wf.run('MultiProc', plugin_args={'n_procs': n_procs})

if __name__ == '__main__':
    unittest.main()
