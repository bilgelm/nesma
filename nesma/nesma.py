import numpy as np
from tqdm import tqdm

def NESMA_semiadaptive(img, mask, window_half_size, thresh = 0.05):
    '''
    Nonlocal estimation of multispectral magnitudes

    Bouhrara M, Reiter DA, Maring MC, Bonny JM, Spencer RG. Use of the NESMA
    Filter to Improve Myelin Water Fraction Mapping with Brain MRI.
    J Neuroimaging. 2018;28(6):640–9.

    Args:
        img (numpy.ndarray): 4-D image array
        mask (numpy.ndarray): 3-D boolean mask indicating where smoothing should
                              be performed
        window_half_size (list of int): the size of search window centered
                                        around a voxel will be
                                        2 * window_half_size + 1
                                        (any part of the window that extends
                                        beyond the image will be truncated)
        thresh (float): a value between 0 and 1, optional (default = 0.05)

    Returns:
        S_NESMA (numpy.ndarray): filtered output image, 4-D
        VSM (numpy.ndarray): voxel similarity map, 3-D
                             indicates the number of voxels in the search
                             window that were below the threshold
    '''
    if thresh<0 or thresh>1:
        raise ValueError('thresh must be between 0 and 1')
    if img.ndim!=4:
        raise ValueError('input image must be 4-D')
    if mask.ndim!=3:
        raise ValueError('mask must be 3-D')
    if not np.all(img.shape[:-1]==mask.shape):
        raise ValueError('input image and mask shapes must be compatible')

    m, n, o, p = img.shape

    if m <= window_half_size[0] or \
       n <= window_half_size[1] or \
       o <= window_half_size[2]:
        raise ValueError('image size should be greater than window_half_size')

    S_NESMA = np.copy(img)
    VSM = np.zeros(img.shape[:-1])

    indices = np.where(mask)

    for i,j,k in tqdm(zip(*indices), total=np.sum(mask)):
        tmin = max(k - window_half_size[2], 0)
        tmax = min(k + window_half_size[2] + 1, o)

        rmin = max(i - window_half_size[0], 0)
        rmax = min(i + window_half_size[0] + 1, m)

        # index voxel signal vector
        x = img[i:i+1,j:j+1,k:k+1,:]

        smin = max(j - window_half_size[1], 0)
        smax = min(j + window_half_size[1] + 1, n)

        # get the neighborhood around voxel
        nbhd = img[rmin:rmax, smin:smax, tmin:tmax, :]

        # relative Manhattan distance calculation
        #
        # in the NESMA paper, the denominator includes the L1 norm of x only
        # here, we use the sum of the L1 norm of x and the L1 norm of the voxel
        # x is being compared to
        # (plus a very small number to prevent division by 0)
        #
        # this choice of denominator is motivated by the inequality that
        # |y - x| <= |x| + |y|
        # this way, the threshold is bounded above by 1
        D = np.sum(np.abs(nbhd - x), axis=-1) / \
            (np.sum(np.abs(x)) + np.sum(np.abs(nbhd), axis=-1) + np.finfo(float).eps)

        pos = D < thresh
        n_below_thresh = np.sum(pos)
        if n_below_thresh>0:
            S_NESMA[i,j,k,:] = np.sum(nbhd[pos,:], axis=0) / n_below_thresh
            VSM[i,j,k] = n_below_thresh

    return S_NESMA, VSM
