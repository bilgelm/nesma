import unittest
import numpy as np
from nesma import *

class TestNESMA(unittest.TestCase):
    def setUp(self):
        img_size = (12, 11, 10)
        self.img = np.zeros(img_size)
        self.img[3:9, 4:8, 5:7] = 5

        self.img = np.tile(self.img[...,np.newaxis], 3)
        self.mask = np.ones(img_size, dtype='bool')

        # add noise
        self.img += np.random.rand(*self.img.shape)

    def test_NESMA_semiadaptive(self):
        window_half_size = [3,2,1]
        thresh = 0.05

        S_NESMA, VSM = NESMA_semiadaptive(self.img, self.mask,
                                          window_half_size, thresh)

if __name__ == '__main__':
    unittest.main()
