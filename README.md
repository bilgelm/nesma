# NESMA: Nonlocal estimation of multispectral magnitudes

This is a python package implementing the NESMA filter as described in:

[Bouhrara M, Reiter DA, Maring MC, Bonny JM, Spencer RG.
Use of the NESMA Filter to Improve Myelin Water Fraction Mapping with Brain MRI.
J Neuroimaging. 2018;28(6):640–9.](https://onlinelibrary.wiley.com/doi/full/10.1111/jon.12537)

## Installation
Clone the repository to your computer. Then, install this package in your
python environment using:
```
pip install -e <PATH_TO_NESMA_REPO>[nipype]
```
where `<PATH_TO_NESMA_REPO>` is the path to the cloned repository.
The `[nipype]` at the end specifies that the additional libraries required for
the nipype wrappers for NESMA should also be installed.

## Running tests
Navigate to the repository directory.

To run tests in a specific file, run:
```
python tests/test_nesma.py
```

To run all tests, run:
```
python -m unittest discover tests/
```
<span style="color:red">Running all tests will be very time consuming because
the test in `test_nipype.py` will take over 2 hours to run with the current
implementation.</span>
(Currently, the GitLab CI is configured to ignore `test_nipype.py`.)

## Software environment
This software has minimal module dependencies, so you should easily be able to
run it in your conda environment. If you need a containerized environment, you
can use:
```
singularity shell docker://python:latest
```

This is the container used for GitLab CI testing.
