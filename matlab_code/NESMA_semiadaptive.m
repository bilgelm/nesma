function c=NESMA_semiadaptive(raw,txy,tz,thresh)

[m,n,o,p]=size(raw); 
S_NESMA=single(zeros(size(raw))); %Filtered output image
VSM=single(zeros(m,n,o)); %Voxel similarity map

for k=1:o
    disp(o-k);
    for i=1:m
        for j=1:n
            
            rmin=max(i-txy,1);rmax=min(i+txy,m);
            smin=max(j-txy,1);smax=min(j+txy,n);
            tmin=max(k-tz,1);tmax=min(k+tz,o);
            L=length(rmin:rmax)*length(smin:smax)*length(tmin:tmax);
            
            rawi=reshape(raw(rmin:rmax,smin:smax,tmin:tmax,:),[L p]);
            x(1,:)=raw(i,j,k,:); %index voxel signal vector
            D=100*sum(abs(bsxfun(@minus,rawi,x)),2)./sum(x); %distance calculation (Relative)
            pos=D<thresh;
            S_NESMA(i,j,k,:)=mean(rawi(pos==1,:),1);
            VSM(i,j,k)=nnz(pos);
        end
    end
end
c(1,1)={S_NESMA};
c(1,2)={VSM};
end


