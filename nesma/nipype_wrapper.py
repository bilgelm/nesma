from nipype.interfaces.base import BaseInterface, BaseInterfaceInputSpec, \
                                   TraitedSpec, File, traits
from nipype.utils.filemanip import split_filename
import nibabel as nib

from nesma import NESMA_semiadaptive
import os

class NESMAInputSpec(BaseInterfaceInputSpec):
    in_file = File(exists=True, mandatory=True, desc='4D image file')
    mask_file = File(exists=True, mandatory=True, desc='3D mask file')
    window_half_size = traits.List(traits.Int(), minlen=3, maxlen=3,
                                   mandatory=True, desc='Window half size')
    thresh = traits.Float(mandatory=True,
                          desc='Relative Manhattan distance threshold')

class NESMAOutputSpec(TraitedSpec):
    out_file = File(exists=True, desc='Filtered 4D image file')
    vsm_file = File(exists=True, desc='3D voxel similarity map')

class NESMA(BaseInterface):
    input_spec = NESMAInputSpec
    output_spec = NESMAOutputSpec

    def _run_interface(self, runtime):
        img = nib.load(self.inputs.in_file)
        mask = nib.load(self.inputs.mask_file)

        res, vsm = NESMA_semiadaptive(img.get_fdata(),
                                      mask.get_fdata(),
                                      self.inputs.window_half_size,
                                      self.inputs.thresh)

        _, base, ext = split_filename(self.inputs.in_file)

        vsm_img = nib.Nifti1Image(vsm, img.affine, img.header)
        vsm_fname = base + '_nesmaVSM' + ext
        nib.save(vsm_img, vsm_fname)

        res_img = nib.Nifti1Image(res, img.affine, img.header)
        res_fname = base + '_nesma' + ext
        nib.save(res_img, res_fname)

        return runtime

    def _list_outputs(self):
        outputs = self._outputs().get()
        _, base, ext = split_filename(self.inputs.in_file)
        outputs['vsm_file'] = os.path.abspath(base + '_nesmaVSM' + ext)
        outputs['out_file'] = os.path.abspath(base + '_nesma' + ext)

        return outputs
